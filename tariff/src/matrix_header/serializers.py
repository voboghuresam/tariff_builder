from rest_framework import serializers
from .models import MatrixHeader


class MatrixHeaderSerializer(serializers.ModelSerializer):
    class Meta:
        model = MatrixHeader
        fields = "__all__"
