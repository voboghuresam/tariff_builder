from django.db import models
from django.contrib.postgres.fields import JSONField
from src.matrix.models import Matrix
# Create your models here.


class MatrixHeader(models.Model):
    HEADER_CHOICES = [
        ('x', 'X-Header'),
        ('y', 'Y-Header'),
    ]
    header_type = models.TextField()
    number_of_rows = models.TextField()
    data = JSONField()
    matrix = models.ForeignKey(
        Matrix, blank=True, null=True, on_delete=models.CASCADE, related_name='matrix_matrix_header')
    header_tag = models.CharField(
        max_length=1,
        choices=HEADER_CHOICES,
        default='x',
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
