from rest_framework.routers import DefaultRouter
from .views import HeaderMatrixView
from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
router = DefaultRouter()
router.register("add", HeaderMatrixView, basename='MatirxHeader')
# router.register("add/<int:pk>", HeaderMatrixView,
#                 basename='MatrixHeaderUpdate'),
urlpatterns = [path("", include(router.urls))]
