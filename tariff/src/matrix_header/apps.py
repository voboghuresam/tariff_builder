from django.apps import AppConfig


class MatrixHeaderConfig(AppConfig):
    name = 'matrix_header'
