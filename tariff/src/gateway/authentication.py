# from rest_framework_simplejwt.tokens import RefreshToken
# from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.authentication import BaseAuthentication
from datetime import datetime
from src.user.models import User
import jwt
from django.conf import settings

""" 
JWT token authentication and validation 
and also verify token
"""


class Authentication(BaseAuthentication):
    def authenticate(self, request):
        data = self.validate_request(request.headers)
        if not data:
            return None
        return self.get_user(data['user_id']), None

    def get_user(self, user_id):
        try:
            user = User.objects.get(id=user_id)
        except Exception:
            return None
        return user

    def validate_request(self, headers):
        authorization = headers.get("Authorization", None)
        if not authorization:
            return None
        token = headers['Authorization'][7:]
        decoded_data = Authentication.verify_token(token)
        if not decoded_data:
            return None
        return decoded_data

    @staticmethod
    def verify_token(token):
        try:
            valid_data = jwt.decode(
                jwt=token, key=settings.SECRET_KEY, algorithms=['HS256'])
            # print(valid_data['exp'])
            # print(int(datetime.now().timestamp()))
            # valid_data = TokenBackend(algorithm='HS256').decode(str(token), verify=False)
            exp = int(valid_data['exp'])
            if datetime.now().timestamp() > exp:
                return None
            # logging.error('lifetime %s', refresh.payload.exp)
            return valid_data
        except Exception as e:
            return e
