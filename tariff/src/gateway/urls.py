from django.urls import include, path
from .views import MyTokenObtainPairView, RegisterView, RefreshView, GetSecureInfo, ExceptionView

urlpatterns = [
    path('login', MyTokenObtainPairView.as_view(), name='login_view'),
    path('register', RegisterView.as_view(), name='register_view'),
    path('refresh', RefreshView.as_view(), name='refresh_view'),
    path('secure-info', GetSecureInfo.as_view(), name='secure_info_view'),
    path('exception', ExceptionView.as_view(), name='exception_view'),
]
