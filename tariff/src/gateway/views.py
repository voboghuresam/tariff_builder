from .models import Jwt
from src.user.models import User
from django.conf import settings
from rest_framework import status
from rest_framework.views import APIView
from django.contrib.auth import authenticate
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework_simplejwt.authentication import JWTAuthentication
import logging
from rest_framework_simplejwt.views import TokenObtainPairView
from .serializers import LoginSerializer, MyTokenObtainPairSerializer, RegisterSerializer, RefreshSerializer
from .authentication import Authentication


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer
    login_serializer = LoginSerializer

    def post(self, request, format=None):
        LoginSerializer = self.login_serializer(data=request.data)
        LoginSerializer.is_valid(raise_exception=True)
        """ authenticate user """
        user = authenticate(
            username=LoginSerializer._validated_data['email'],
            password=LoginSerializer._validated_data['password'])
        if not user:
            return Response({
                "error": "Invalid credentials",
                "status": status.HTTP_404_NOT_FOUND
            })
        Jwt.objects.filter(user_id=user.id).delete()
        refresh = self.serializer_class.get_token(user)
        # logging.error('test :: %s', refresh.access_token)
        Jwt.objects.create(user_id=user.id,
                           access=refresh.access_token,
                           refresh=refresh)
        return Response({
            "access": str(refresh.access_token),
            "refresh": str(refresh)
        })


class RegisterView(APIView):
    serializer_class = RegisterSerializer
    permission_classes = [AllowAny]

    def post(self, request, format=None):
        serrializer = self.serializer_class(data=request.data)
        serrializer.is_valid(raise_exception=True)
        User.objects.create_user(**serrializer._validated_data)
        return Response({
            "success": 'User created successfully',
            "status": status.HTTP_201_CREATED
        })


class RefreshView(APIView):
    serializer_class = RefreshSerializer
    token_obtain_class = MyTokenObtainPairSerializer
    permission_classes = [AllowAny]

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            active_jwt = Jwt.objects.get(
                refresh=serializer._validated_data["refresh"])
        except Jwt.DoesNotExist:
            return Response({
                "error": "Refresh token not found",
                "status": status.HTTP_404_NOT_FOUND
            })
        if Authentication.verify_token(serializer._validated_data["refresh"]):
            return Response({
                'error': "Refresh token is invalid or expired",
                "status": status.HTTP_406_NOT_ACCEPTABLE
            })

        refresh = self.token_obtain_class.get_token(active_jwt.user)
        active_jwt.refresh = str(refresh)
        active_jwt.access = str(refresh.access_token)
        active_jwt.save()

        return Response({
            "access": str(refresh.access_token),
            "refresh": str(refresh)
        })


class GetSecureInfo(APIView):
    authentication_classes = [Authentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        return Response({"test": "secure test"})


class ExceptionView(APIView):
    authentication_classes = [Authentication]
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        try:
            a = 2 / 0
        except Exception as e:
            raise Exception("Can not divide by zero")
        return Response({"test": "exception test"})
