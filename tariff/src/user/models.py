from django.db import models
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser,
                                        PermissionsMixin)
from pprint import pprint
from django.contrib.auth.hashers import make_password, check_password

import logging


class CustomManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        if (not email):
            raise ValueError("Email field Required")
        if (password is None):
            raise TypeError("Password field Required")
        user = self.model(email=self.normalize_email(email),
                          name=extra_fields.get('name'))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        extra_fields.setdefault('is_authenticated', True)
        extra_fields.setdefault('name', "admin")
        if extra_fields.get('is_staff') is not True:
            raise ValueError('SuperUser must have is_staff : true')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('SuperUser must have is_superuser : true')
        return self.create_user(email, password, **extra_fields)


# Create your models here.
class User(models.Model):
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=255)
    password = models.CharField(max_length=128, default=None)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_anonymous = models.BooleanField(default=False)
    is_authenticated = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('name', )

    objects = CustomManager()

    def __str__(self):
        logging.error("Checking email %s", self.email)
        return self.email

    def set_password(self, password):
        self.password = make_password(password)

    def check_password(self, password):
        # logging.error("Checking password %s", self.password)
        # logging.error("Checking password %s", password)
        return check_password(password, str(self.password))

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return self.is_superuser


class AddressGlobal(models.Model):
    address = models.TextField()
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    country = models.CharField(max_length=100)


class UserProfile(models.Model):
    user = models.OneToOneField(User,
                                related_name='user_profile',
                                on_delete=models.CASCADE)

    profile_pic = models.ImageField(upload_to='profile_pics')
    address_info = models.ForeignKey(AddressGlobal,
                                     related_name='user_address',
                                     null=True,
                                     on_delete=models.SET_NULL)

    dob = models.DateTimeField()

    def __str__(self):
        return self.user.email
