from rest_framework.viewsets import ModelViewSet
from .serializers import UserProfileSerializer, UserSerializer, User, UserProfile


# Create your views here.
class UserView(ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.prefetch_related(
        "user_profile",
        "user_profile__address_info",
    )


class UserProfileView(ModelViewSet):
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.select_related("user", "address_info")
