from rest_framework.routers import DefaultRouter
from django.urls import include, path
from .views import UserView, UserProfileView

router = DefaultRouter()
router.register('user', UserView)
router.register('user-profile', UserProfileView)

urlpatterns = [
    path('', include(router.urls)),
]
