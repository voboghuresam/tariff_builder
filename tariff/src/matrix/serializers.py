from rest_framework import serializers
from .models import Matrix
from src.matrix_header.serializers import MatrixHeaderSerializer

"""
Fetch Matrix and its clidrens as array  
"""


class MatrixSerializer(serializers.ModelSerializer):
    # reverse direction
    matrix_matrix_header = MatrixHeaderSerializer(many=True)

    class Meta:
        model = Matrix
        fields = ["id", "number_of_rows", "number_of_columns",
                  "data",  "matrix_matrix_header", "created_at", "updated_at"]
        # deep = 1 for one to many uni direction
