from django.db import models
from django.contrib.postgres.fields import JSONField
# Create your models here.


class Matrix(models.Model):
    number_of_rows = models.TextField()
    number_of_columns = models.TextField()
    data = JSONField()
    # TODO: matrix_header update delete
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
