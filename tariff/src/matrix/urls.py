from rest_framework.routers import DefaultRouter
from django.urls import path, include
from .views import MatrixView
router = DefaultRouter()
router.register("add", MatrixView, basename='Matrix')
urlpatterns = [path("", include(router.urls))]
