from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.viewsets import ModelViewSet
from rest_framework import permissions
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from .serializers import MatrixSerializer
from .models import Matrix
from django.db.models import Prefetch


class MatrixView(ModelViewSet):
    serializer_class = MatrixSerializer
    queryset = Matrix.objects.prefetch_related("matrix_matrix_header")
    permission_classes = [permissions.IsAuthenticated]

    """ 
    Create custom matrix 
    """

    def create(self, request, *args, **kwargs):
        serializer = MatrixSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            response_data = {
                'status': 'success',
                'data': serializer.data,
                'message': 'Matrix created.',
            }
            return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    """ 
    Update custom matrix. 
    """

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.serializer_class(
            data=request.data, instance=instance, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(self.serializer_class(self.get_object()).data)
